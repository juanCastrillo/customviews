package com.juan.customviews

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import kotlin.concurrent.thread
import kotlin.coroutines.CoroutineContext
import android.os.Looper
import com.google.android.material.textfield.TextInputEditText

//TODO Fix crappy design like button placement and aligning and extras
class TestActivity:AppCompatActivity() {

    private lateinit var materialLoadingBar:MaterialLoadingBar
    private lateinit var selectors: RoundSelectors
    private lateinit var changeSizeButton: MaterialButton
    private lateinit var resetButton:MaterialButton
    private lateinit var togglePercent:MaterialButton
    private lateinit var stopStartButton:MaterialButton
    private lateinit var k:MaterialButton
    private lateinit var changePercentage:MaterialButton
    private lateinit var editTextPercentage:TextInputEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        materialLoadingBar = findViewById(R.id.materialLoadingBar)
        selectors = findViewById(R.id.selectors)
        changeSizeButton = findViewById(R.id.changeSize)
        resetButton = findViewById(R.id.reset)
        togglePercent = findViewById(R.id.togglePercent)
        stopStartButton = findViewById(R.id.stopStart)
        changePercentage = findViewById(R.id.changePercentage)
        editTextPercentage = findViewById(R.id.textEditPercentage)

        selectors.changeSelectorColor(0, Color.valueOf(23F,23F,250F))

        var i:Double = 0.0
        var shouldContinue = true



        val k = Thread {
            while(true){
                if(shouldContinue){
                    if(i>100) i = 0.0

                    Utils.runOnUiThread(Runnable{materialLoadingBar.updatePercentage((i/100).toFloat())})
                    i+=1.0
                }
                Thread.sleep(1000)

            }

        }
        k.start()


        changeSizeButton.setOnClickListener { if(materialLoadingBar.textSize > 20) materialLoadingBar.textSize = 20f else materialLoadingBar.textSize = 500f}
        resetButton.setOnClickListener { materialLoadingBar.setSmallPercentage(0.0f); i = 0.0}
        togglePercent.setOnClickListener { materialLoadingBar.showPercentage = !materialLoadingBar.showPercentage }
        stopStartButton.setOnClickListener {
            //materialLoadingBar.percentage = 0.5
            try{

                if(k.isAlive) {
                    if (!shouldContinue) stopStartButton.text = "Stop" else stopStartButton.text = "Start"
                        shouldContinue = !shouldContinue
                }
                else k.start()

            } catch (e:Exception){ Log.d("interruptException", e.toString())}
        }
        changePercentage.setOnClickListener {  materialLoadingBar.updatePercentage(editTextPercentage.text.toString().toFloat()) }

    }

}