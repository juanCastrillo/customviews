package com.juan.customviews

import android.os.Handler
import android.os.Looper

class Utils {

    companion object {
        fun runOnUiThread(runnable: Runnable) {
            val uiHandler = Handler(Looper.getMainLooper())
            uiHandler.post(runnable)
        }
    }
}