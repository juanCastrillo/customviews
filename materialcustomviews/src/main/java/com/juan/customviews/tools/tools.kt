package com.juan.customviews.tools

import android.content.Context
import android.util.DisplayMetrics
import com.juan.customviews.data.Position


fun Float.toPixels(context: Context): Float = this *
        (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)

fun Int.toPosition(): Position = when(this){
    0 -> Position.LEFT
    1 -> Position.RIGHT
    2 -> Position.MIDDLE
    else -> Position.MIDDLE
}