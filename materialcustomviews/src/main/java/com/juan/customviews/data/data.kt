package com.juan.customviews.data

data class Dimensions(var width:Int = 0, var height:Int = 0)

enum class Position(value:Int) {
    LEFT(0),
    RIGHT(1),
    MIDDLE(2)
}