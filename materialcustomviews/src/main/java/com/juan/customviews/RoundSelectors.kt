package com.juan.customviews

import android.content.Context
import android.content.res.TypedArray
import android.view.View
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

/**
 * CUSTOM VIEW BECOUSE WHY NOT
 */
class RoundSelectors(ct: Context, attrs: AttributeSet): View(ct, attrs) {

    data class Selector(
        var circle:Circle,
        var name:String
    ){
        val getCircle:Circle
            get() = circle
    }

    data class Circle(
        var cx:Float, //x coordinate of the center
        var cy:Float, //y coordinate of the center
        var radius:Float,
        var paint:Paint
    )

    val paint = Paint()

    private lateinit var selectors:List<Selector>

    fun changeSelectorColor(i:Int, c:Color){
        if(i < selectors.size)
            selectors[i].circle.paint.color = c.toArgb()
    }

    fun changeSelectorName(i:Int, n:String){
        if(i < selectors.size)
            selectors[i].name = n
    }

    var selectorCount:Int

    var selectorWidth:Int = 0
    var selectorHeight:Int = 0
    var padding = 0

    val a: TypedArray

    init {
        a = context.theme.obtainStyledAttributes(attrs, R.styleable.Selector, 0, 0)

        selectorCount = a.getInt(R.styleable.Selector_selectorCount, 3)
        if(selectorCount<3) selectorCount = 3

        initializeSelectors()
        //Log.d("selectorCount", selectorCount.toString())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        paint.isAntiAlias = true
        paint.color = Color.BLACK

        canvas.apply {
            selectors.forEach {
                drawCircle(it.getCircle)
                drawText(it.name, it.circle.cx-it.circle.radius-padding, selectorHeight-padding.toFloat(), paint)
                //drawRect(Rect(0,0,2000,1000), paint)
            }

        }

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        Log.d("selectorCount",selectorCount.toString()+"k")
        val width = View.MeasureSpec.getSize(widthMeasureSpec)
        val height = View.MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(width/selectorCount, View.MeasureSpec.EXACTLY))


        initializeSelectors()
    }

    private fun initializeSelectors() {
        selectors = List(selectorCount) {
            Selector(
                circle = Circle(it*selectorWidth.toFloat(), selectorHeight.toFloat(), selectorWidth/2F-padding,
                    Paint(Paint.ANTI_ALIAS_FLAG).apply {
                        style = Paint.Style.FILL
                        color = a.getColor(R.styleable.Selector_allSelectorsColor, ContextCompat.getColor(context,R.color.colorPrimary))
                    }),
                name = ""
            )}

        Log.d("selectorHeigh", selectorHeight.toString())
        Log.d("selectorWidth", measuredWidth.toString())

        Log.d("circleidk", "${selectors[0].circle.cx} x ${selectors[0].circle.cy}")
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        selectorWidth = w/selectorCount
        selectorHeight = h/2
        padding = (selectorWidth*0.1).toInt()


    }

}

fun Canvas.drawCircle(circle: RoundSelectors.Circle) = this.drawCircle(circle.cx, circle.cy, circle.radius, circle.paint)