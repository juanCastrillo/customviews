package com.juan.customviews

import android.animation.*
import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.juan.customviews.data.Dimensions
import android.graphics.Paint.Align
import android.icu.util.Measure
import android.view.KeyEvent
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import androidx.core.view.ViewCompat.getClipBounds
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import com.juan.customviews.data.Position
import com.juan.customviews.tools.toPosition
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import kotlin.concurrent.thread


/**
 * CustomView to represent a percentage
 * shape of a bar
 *
 * @percentaje can be hidden and put in either center, left or right inside or out
 * @cornerRadius the bar can be made round to allow for a smother design
 * @percentaje when change the bar animates to show the change
 *
 */
class MaterialLoadingBar(ct: Context, attrs: AttributeSet): View(ct, attrs){

    //TODO set proper max and mins
    val maxHeight = 200
    var minHeight:Float
    val minWidth = 200

    var progressBarColor: Int = colorize(R.color.progressBarDefault)
        set(value) {field = value; progressBarPaint.color = field; updateView()}
    var staticBarColor:Int = colorize(R.color.noneStaticBar)
        set(value) {field = value; staticBarPaint.color = field; updateView()}
    var outlineColor:Int = Color.BLACK
        set(value) {field = value; outlinePaint.color = field; updateView()}
    
    
    var outlineThickness:Float = 2f
        set(value) {field = value; outlinePaint.strokeWidth = field; updateView()}
    

    var cornerRadius: Float
    var textInside:Boolean = false
        set(value) {field = value; updateView()}

    var textSize: Float = 50f
        set(value) {field = value; textPaint.textSize = value; updateView() }

    var textColor: Int = Color.BLACK
        set(value) {field = value; textPaint.color = value;}

    var paddingBetween:Int = 0
        set(value) { field = value; updateView()}

    var showPercentage: Boolean = false
        set(value) { field = value; updateView()}
    var showStaticBar:Boolean = false
        set(value) {field = value; updateView()}
    var showOutline:Boolean = true
        set(value) {field = value; updateView()}

    private var animationReady: Boolean = true

    /**
     * Percentage of fullness of the loading bar
     */
    private var percentage:Float = 0f

    /**
     * set the percentage of progress
     * set any value from 0 to 1
     */
    fun setSmallPercentage(percentage: Float) {this.percentage = percentage; progressBarRectUpdate(); invalidate()}//startAnimation(field.toFloat())}//

    /**
     * sets the percentage of progress
     * set any value from 0 to 100
     */
    fun setBigPercentage(percentage: Float) {this.percentage = percentage/100; progressBarRectUpdate(); invalidate()}//startAnimation(field.toFloat())}//

    /**
     * animate bar movement setting percentage (1/5 second duration)
     * @param percentage from 0.00 to 1.00
     */
    //TODO manage working with more than 1 update request in the animation execution
    fun updatePercentage(percentage: Float) { startAnimation(percentage); this.percentage = percentage}//thread {  while(!animationReady) Thread.sleep(100) startAnimation(percentage) }}

    var percentagePosition: Position = Position.LEFT
        set(pos: Position) {field = pos; updateView()}

    private val paddingVertical: Int
    private val paddingHorizontal: Int

    private var percentageDimensions: Dimensions = Dimensions()
    private var barDimensions: Dimensions = Dimensions()

    private val progressBarPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = progressBarColor
    }

    private val staticBarPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = staticBarColor
    }

    private val outlinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = outlineColor
        strokeWidth = outlineThickness
    }

    private lateinit var percentageRect: RectF
    private lateinit var staticBarRect: RectF
    private lateinit var progressBarRect: RectF

    private var textPaint:TextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
        color = textColor
        textSize = textSize
        //textAlign = Paint.Align.CENTER
    }

    init {

        paddingVertical = paddingBottom+paddingTop
        paddingHorizontal =  paddingStart+paddingEnd

        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.MaterialLoadingBar,
            0, 0).apply {

            try {
                progressBarColor = getColor(R.styleable.MaterialLoadingBar_color_progressbar, colorize( R.color.progressBarDefault))
                staticBarColor = getColor(R.styleable.MaterialLoadingBar_color_staticbar, colorize(R.color.softGrey))
                outlineColor = getColor(R.styleable.MaterialLoadingBar_color_outline, Color.BLACK)

                showPercentage = getBoolean(R.styleable.MaterialLoadingBar_showPercentage, true)
                showStaticBar = getBoolean(R.styleable.MaterialLoadingBar_showStaticBar, false)
                showOutline = getBoolean(R.styleable.MaterialLoadingBar_showOutline, true)

                outlineThickness = getDimensionPixelSize(R.styleable.MaterialLoadingBar_outline_thickness, 2).toFloat()
                percentagePosition = getInteger(R.styleable.MaterialLoadingBar_percentagePosition, 0).toPosition()
                cornerRadius = getDimensionPixelSize(R.styleable.MaterialLoadingBar_cornerRadius, 0).toFloat()
                textSize = getDimensionPixelSize(R.styleable.MaterialLoadingBar_textSize, 50).toFloat()
                textColor = getColor(R.styleable.MaterialLoadingBar_textColor, Color.BLACK)
                paddingBetween = getDimensionPixelSize(R.styleable.MaterialLoadingBar_paddingBetween, 0)
                textInside = getBoolean(R.styleable.MaterialLoadingBar_percentageInside, false)
                

            } finally { recycle() }

            textPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
                color = Color.BLACK
                color = textColor
                textSize = textSize
                //textAlign = Paint.Align.CENTER
            }


            if(!showStaticBar)
                staticBarColor = R.color.noneStaticBar

            //Log.d("tama;oTexto", texttextSize.toString())
        }

        minHeight = textSize
        Log.d("minHeight", minHeight.toString())

        //startAnimation(0.5.toFloat());
    }

    private val shadowPaint = Paint(0).apply {
        color = 0x101010
        maskFilter = BlurMaskFilter(8f, BlurMaskFilter.Blur.NORMAL)
    }

    private val whitePaint = Paint().apply {
        color = Color.WHITE
    }

    private val blackPaint = Paint().apply {
        color = Color.BLACK
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {

        val w = MeasureSpec.getSize(widthMeasureSpec)-paddingHorizontal
        val h = MeasureSpec.getSize(heightMeasureSpec)-paddingVertical

        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        Log.d("actualHeight", h.toString())

        val finalHeight:Int =
            if(heightMode == MeasureSpec.AT_MOST)
                minHeight.toInt()
            else
                when {
                    h > maxHeight -> maxHeight
                    h < minHeight -> minHeight.toInt()
                    else -> h
                }

        val finalWidth =
            if(widthMode == MeasureSpec.AT_MOST) minWidth
            else
                when {
                    w < minWidth -> minWidth
                    else -> w
                }

        Log.d("size", "$finalWidth x $finalHeight")

        setMeasuredDimension((finalWidth+paddingHorizontal).toMeasuredSpec(),(finalHeight+paddingVertical).toMeasuredSpec())
        updateView()

    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateView()
    }

    private fun updateView(){

        if(showPercentage) {
            percentageDimensions.height = height - paddingVertical
            percentageDimensions.width = textPaint.measureText("100.0%").toInt()
            Log.d("percentajeWidth", percentageDimensions.width.toString())
        }
        else{
            percentageDimensions.height = 0
            percentageDimensions.width = 0
        }

        barDimensions.height = height-paddingVertical//*0.5).toInt()
        val extra:Int =
            if(showPercentage && percentagePosition!=Position.MIDDLE && !textInside) (paddingBetween + percentageDimensions.width)
            else 0
        barDimensions.width = width - paddingHorizontal - extra

        createRects()

        progressBarRectUpdate()

        invalidate()
    }

    private fun createRects() {

        val middleVertical = height/2
        val topBarRect = middleVertical-barDimensions.height/2.toFloat()
        val bottomBarRect = middleVertical+barDimensions.height/2.toFloat()

        when(percentagePosition){
            Position.LEFT -> {
                val offset:Float =
                    if(showPercentage) {
                        percentageRect = RectF(Rect(
                            if(textInside) paddingStart+paddingBetween else paddingStart,
                            paddingTop,
                            paddingStart + percentageDimensions.width,
                            paddingTop + percentageDimensions.height
                        ))

                        percentageRect.right + paddingBetween
                    }
                    else {
                        percentageRect = RectF(0f,0f,0f,0f)
                        paddingStart.toFloat()
                    }

                staticBarRect = RectF(offset, topBarRect,
                    offset +barDimensions.width, bottomBarRect)
            }
            Position.RIGHT -> {

                staticBarRect = RectF(paddingStart.toFloat(), topBarRect, paddingStart+barDimensions.width.toFloat(), bottomBarRect)

                if(showPercentage) {
                    val start = if(textInside) staticBarRect.right - percentageDimensions.width - paddingBetween
                    else staticBarRect.right+paddingBetween
                    percentageRect = RectF(start, paddingTop.toFloat(), start + percentageDimensions.width, paddingTop+percentageDimensions.height.toFloat())
                }

            }

            Position.MIDDLE -> {

                staticBarRect = RectF(paddingStart.toFloat(), topBarRect,
                    paddingStart+barDimensions.width.toFloat(), bottomBarRect)
                if(showPercentage) {
                    val mid = width/2
                    val side = percentageDimensions.width/2
                    percentageRect = RectF(mid-side.toFloat(), paddingTop.toFloat(), mid+side.toFloat() + percentageDimensions.width, paddingTop+percentageDimensions.height.toFloat())
                }
            }
        }

        if(textInside) staticBarRect = RectF(paddingStart.toFloat(), topBarRect, (paddingStart + barDimensions.width).toFloat(), bottomBarRect)
        Log.d("rectanguloBarra",staticBarRect.toString())
        //progressBarRectUpdate()
    }

    private fun progressBarRectUpdate(){

        val originalSize = try{
            progressBarRect.right
        } catch (e:Exception){ (staticBarRect.right*percentage).toInt() }


        val finalSize = ((staticBarRect.right-staticBarRect.left)*percentage).toInt() + staticBarRect.left
        //Log.d("positionBar", "${staticBarRect.right}*$percentage = $finalSize")

        progressBarRect = RectF(staticBarRect.left, staticBarRect.top, finalSize, staticBarRect.bottom)
        //invalidate()
        sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED)

    }

    private fun Int.toMeasuredSpec() = View.MeasureSpec.makeMeasureSpec(this, MeasureSpec.EXACTLY)

    private val r = Rect()


    private fun drawTextVerticalCenter(canvas: Canvas, paint: TextPaint, text: String, xStart:Float) {

        val oneCeroSize = textPaint.measureText("0")
        val twoCeroSize = textPaint.measureText("00")

        val number:Double = try {
            text.replace("%","").toDouble()
        } catch (e:Exception) {Log.e("notNumberProvided", e.toString()); 100.0}

        canvas.getClipBounds(r)
        val cHeight = r.height()
        //val cWidth = r.width()
        paint.textAlign = Paint.Align.LEFT
        paint.getTextBounds(text, 0, text.length, r)
        //val x = cWidth / 2f - r.width() / 2f - r.left
        val y = cHeight / 2f + r.height() / 2f - r.bottom

        //TODO fix percentaje not centered offset too big
        val xYes:Float =

            if (percentagePosition == Position.MIDDLE) {
                when {
                    number < 100.0 && number >= 10.0 -> xStart + oneCeroSize
                    number < 10.0 -> xStart + twoCeroSize
                    else -> xStart
                }
            } else xStart

        canvas.drawText(text, xYes, y, paint)
    }

    val path = Path()

    /**
     *
     */
    //TODO make container to limit the progress bar size
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        //TODO draw behind the staticBarRectRoundArea
        path.addRoundRect(staticBarRect, cornerRadius, cornerRadius, Path.Direction.CW)

        canvas?.apply {

            //drawRect(0F,0F,3000F,2000F, blackPaint)
            //drawLine(0F,height/2-textPaint.textSize/2, 3000F,height/2-textPaint.textSize/2+1, whitePaint)
            //drawLine(0F,height/2+textPaint.textSize/2, 3000F,height/2+textPaint.textSize/2+1, whitePaint)

            //STATIC BAR
            if(showStaticBar) drawRoundRect(staticBarRect, cornerRadius, cornerRadius, staticBarPaint)
            //OUTLINE

            save()
            clipPath(path)

            //LOADING BAR
            drawRect(progressBarRect, progressBarPaint)
            restore()

            if(showOutline) drawRoundRect(staticBarRect, cornerRadius, cornerRadius, outlinePaint)

            //PERCENTAGE TEXT
            if(showPercentage)
                drawTextVerticalCenter(canvas, textPaint, "${String.format("%.1f",percentage*100)}%", percentageRect.left)

        }

    }

    //TODO call this in the setter of the percentage
    fun startAnimation(percentage:Float){
        val xStart = progressBarRect.right
        val xEnd= staticBarRect.right*percentage
        val animator = ValueAnimator.ofFloat(xStart, xEnd).apply {
            duration = 200
            addUpdateListener {
                    animation ->
                run {
                    val value = animation.animatedValue as Float
                    //Log.d("barSizeAnim", value.toString())
                    progressBarRect.right = value
                    invalidate()
                }
            }
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    animationReady = true
                }
            })
        }
        animator.start()

    }

    override fun onPopulateAccessibilityEvent(event: AccessibilityEvent) {
        super.onPopulateAccessibilityEvent(event)
        // We call the super implementation to populate its text for the
        // event. Then we add our text not present in a super class.
        // Very often you only need to add the text for the custom view.
        event.text.add(percentage.toString())

    }

    override fun onInitializeAccessibilityEvent(event: AccessibilityEvent) {
        super.onInitializeAccessibilityEvent(event)
        // We call the super implementation to let super classes
        // set appropriate event properties. Then we add the new property
        // (checked) which is not supported by a super class.

    }

    override fun onInitializeAccessibilityNodeInfo(info: AccessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(info)
        // We call the super implementation to let super classes set
        // appropriate info properties. Then we add our properties
        // (checkable and checked) which are not supported by a super class.

        // Quite often you only need to add the text for the custom view.
        info.isClickable = false
    }

    private fun colorize(id:Int):Int = ContextCompat.getColor(context, id)


}